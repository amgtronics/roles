<?php

namespace Amgtronics\RolesModels;

use Amgtronics\RolesTraits\Slugable;
use Illuminate\Database\Eloquent\Model;
use Amgtronics\RolesTraits\PermissionHasRelations;
use Amgtronics\RolesContracts\PermissionHasRelations as PermissionHasRelationsContract;

class Permission extends Model implements PermissionHasRelationsContract
{
    use Slugable, PermissionHasRelations;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'slug', 'description', 'model'];

    /**
     * Create a new model instance.
     *
     * @param array $attributes
     * @return void
     */
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);

        if ($connection = config('roles.connection')) {
            $this->connection = $connection;
        }
    }
}
